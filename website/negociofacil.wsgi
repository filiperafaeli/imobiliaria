import os, sys
sys.path.append('/home/imobiliarianegociofacil/apps_wsgi')
sys.path.append('/home/imobiliarianegociofacil/apps_wsgi/negociofacil')
os.environ['PYTHON_EGG_CACHE'] = '/home/imobiliarianegociofacil/apps_wsgi/.python-eggs'
os.environ['DJANGO_SETTINGS_MODULE'] = 'negociofacil.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
