from django import forms

from website.core.models import Tipo, Categoria, Cidade, Bairro


class ContatoForm(forms.Form):
    nome = forms.CharField(required=True)
    telefone = forms.CharField(required=True)
    email = forms.EmailField()
    mensagem = forms.CharField(required=True, widget=forms.Textarea)


VALOR_CHOICES = (
    ('todos', 'Qualquer valor'),
    ('100000', '100.000,00'),
    ('200000', '200.000,00'),
    ('300000', '300.000,00'),
    ('400000', '400.000,00'),
    ('500000', '500.000,00'),
    ('600000', '600.000,00'),
    ('acima', 'Acima de 600.000,00'),
)


class BuscaForm(forms.Form):
    def __init__(self, *args, **kwargs):
        tipos = Tipo.objects.values('tipo').values_list()
        categorias = Categoria.objects.values('descricao').values_list()
        cidades = Cidade.objects.values('nome').values_list()
        bairros = list(Bairro.objects.values('nome').values_list().order_by('nome'))
        bairros.insert(0, tuple({'todos', 'Todos'}))
        super(BuscaForm, self).__init__(*args, **kwargs)
        self.fields["tipo"] = forms.ChoiceField(choices=tipos)
        self.fields["categoria"] = forms.ChoiceField(choices=categorias)
        self.fields["cidade"] = forms.ChoiceField(choices=cidades)
        self.fields["bairro"] = forms.ChoiceField(choices=bairros)
        self.fields["preco"] = forms.ChoiceField(label='Até (R$)', choices=VALOR_CHOICES)

    tipo = forms.CharField(required=True)
    categoria = forms.CharField(required=True)
    cidade = forms.CharField(required=True)
    bairro = forms.CharField(required=True)


class BuscaCodigoForm(forms.Form):
    codigo = forms.CharField(label='Código', required=True)


class ContatoImovelForm(forms.Form):
    nome = forms.CharField(required=True)
    telefone = forms.CharField(required=True)
    email = forms.EmailField(required=True, label='e-mail')
    id_imovel = forms.CharField(widget=forms.HiddenInput(), required=True)
