from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic import TemplateView

from website.core.forms import ContatoForm, BuscaForm, ContatoImovelForm, BuscaCodigoForm
from website.core.models import Imovel, Categoria, Tipo, Interessados, Contato, Banner, Institucional


class Home(TemplateView):

    def get(self, request, *args, **kwargs):
        form = BuscaForm(data=request.POST or None)
        form_codigo = BuscaCodigoForm(data=request.POST or None)
        imoveis = Imovel.objects.filter(visivel=True).order_by("-data")[:8]
        categorias = Categoria.objects.all()
        tipos = Tipo.objects.all()
        institucional, _ = Institucional.objects.get_or_create(pk=1)
        banners_cell = Banner.objects.all().filter(celular=True)
        banners_desk = Banner.objects.all().filter(celular=False)
        return render(request, 'core/index.html',
                      {'imoveis': imoveis, 'categorias': categorias, 'tipos': tipos, 'form': form,
                       'form_codigo': form_codigo,
                       'banner_cell': banners_cell, 'banner_desk': banners_desk, 'institucional': institucional})

    def post(self, request):
        institucional, _ = Institucional.objects.get_or_create(pk=1)
        form = BuscaForm(data=request.POST or None)
        form_codigo = BuscaCodigoForm(data=request.POST or None)
        imoveis = Imovel.objects.filter(visivel=True).order_by("-data")[:8]
        categorias = Categoria.objects.all()
        tipos = Tipo.objects.all()
        if form.is_valid():
            tipo_f = request.POST.get('tipo')
            categoria_f = request.POST.get('categoria')
            cidade_f = request.POST.get('cidade')
            bairro_f = request.POST.get('bairro')
            preco_ff = request.POST.get('preco')

            if 'todos' == bairro_f or 'Todos' == bairro_f:
                imoveis = Imovel.objects.filter(tipo=tipo_f).filter(categoria=categoria_f).filter(
                    cidade=cidade_f).filter(visivel=True)
            else:
                imoveis = Imovel.objects.filter(tipo=tipo_f).filter(categoria=categoria_f).filter(
                    cidade=cidade_f).filter(bairro=bairro_f).filter(visivel=True)

            if preco_ff == 'acima':
                imoveis = imoveis.filter(preco__range=(600000, 99999999)).filter(visivel=True)
            elif preco_ff == 'todos' or preco_ff == 'Qualquer valor':
                imoveis = imoveis
            else:
                imoveis = imoveis.filter(preco__range=(0, preco_ff)).filter(visivel=True)
            return render(request, 'core/index.html',
                          {'imoveis': imoveis, 'categorias': categorias, 'tipos': tipos, 'form': form,
                           'form_codigo': form_codigo,
                           'institucional': institucional})
        else:
            pass
        if form_codigo.is_valid():
            codigo = request.POST.get('codigo')
            imoveis = Imovel.objects.filter(codigo=codigo).filter(visivel=True)

        return render(request, 'core/index.html',
                      {'imoveis': imoveis, 'categorias': categorias, 'tipos': tipos, 'form': form,
                       'form_codigo': form_codigo,
                       'institucional': institucional})


def sobre(request):
    institucional, _ = Institucional.objects.get_or_create(pk=1)
    categorias = Categoria.objects.all()
    tipos = Tipo.objects.all()
    return render(request, 'core/sobre.html',
                  {'categorias': categorias, 'tipos': tipos, 'institucional': institucional})


def contato(request):
    institucional, _ = Institucional.objects.get_or_create(pk=1)
    form_class = ContatoForm
    categorias = Categoria.objects.all()
    tipos = Tipo.objects.all()
    if request.method == 'GET':
        return render(request, 'core/contato.html', {
            'categorias': categorias,
            'tipos': tipos,
            'form': form_class, 'institucional': institucional
        })

    if request.method == 'POST':
        nome = request.POST.get('nome', '')
        email = request.POST.get('email', '')
        telefone = request.POST.get('telefone', '')
        mensagem = request.POST.get('mensagem', '')

        c = Contato.objects.create(nome=nome, email=email, telefone=telefone, mensagem=mensagem)
        c.save()

        return redirect('contato')


def detalhes(request, id_imovel):
    institucional, _ = Institucional.objects.get_or_create(pk=1)
    form_class = ContatoImovelForm(initial={'id_imovel': id_imovel})
    if request.method == 'GET':
        imovelSelecionado = Imovel.objects.get(id=id_imovel)
        categorias = Categoria.objects.all()
        tipos = Tipo.objects.all()
        return render(request, 'core/detalhes.html',
                      {
                          'imovel': imovelSelecionado,
                          'categorias': categorias,
                          'tipos': tipos,
                          'lat': imovelSelecionado.latitude,
                          'long': imovelSelecionado.longitude,
                          'form': form_class, 'institucional': institucional
                      })
    if request.method == 'POST':
        id = request.POST.get('id_imovel', '')

        imv = Imovel.objects.get(id=id)
        nome = request.POST.get('nome', '')
        email = request.POST.get('email', '')
        telefone = request.POST.get('telefone', '')

        i = Interessados.objects.create(nome=nome, email=email, telefone=telefone, imovel=imv)
        i.save()

        return redirect('detalhes', imv.id)


def lista(request, tipo_imovel, categoria_imovel):
    institucional, _ = Institucional.objects.get_or_create(pk=1)
    tipos = Tipo.objects.get(id=tipo_imovel)
    categorias = Categoria.objects.get(id=categoria_imovel)
    imoveis = Imovel.objects.filter(tipo=tipos, categoria=categorias, visivel=True)
    categorias_lista = Categoria.objects.all()
    tipos_lista = Tipo.objects.all()
    return render(request, 'core/lista.html',
                  {'imoveis': imoveis,
                   'categorias': categorias_lista,
                   'tipos': tipos_lista, 'institucional': institucional})


def Busca(request, *args, **kwargs):
    institucional, _ = Institucional.objects.get_or_create(pk=1)
    if request.method == 'GET':
        form = BuscaForm
        return render(request, 'core/busca.html', {'form': form, 'institucional': institucional})
    if request.method == 'POST':
        return render(request, 'core/busca.html')


def todos(request):
    institucional, _ = Institucional.objects.get_or_create(pk=1)
    imoveis = Imovel.objects.filter(visivel=True).order_by("-data")
    categorias_lista = Categoria.objects.all()
    tipos_lista = Tipo.objects.all()
    return render(request, 'core/lista.html',
                  {'imoveis': imoveis,
                   'categorias': categorias_lista,
                   'tipos': tipos_lista, 'institucional': institucional})


def telao1(request):
    imoveis = Imovel.objects.filter(visivel=True).order_by("-data")[:10]
    return render(request, 'core/telao1.html', {'imoveis': imoveis})


def telao2(request):
    imoveis = Imovel.objects.filter(visivel=True).order_by("-data")[:10]
    return render(request, 'core/telao2.html', {'imoveis': imoveis})
