from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'website.core'
    verbose_name = 'Imobiliária'
