from django.contrib import admin
from django.contrib.auth.models import Group

from website.core.models import Imovel, Categoria, Tipo, ImagemImovel, Interessados, Contato, Cidade, Bairro, Banner, \
    Institucional


class ImagemImovelInline(admin.TabularInline):
    model = ImagemImovel
    extra = 1


class NoModuleModelAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        return False


class CidadeModelAdmin(NoModuleModelAdmin):
    pass


class BairroModelAdmin(NoModuleModelAdmin):
    pass


class TipoModelAdmin(NoModuleModelAdmin):
    pass


class CategoriaModelAdmin(NoModuleModelAdmin):
    pass


class ImovelModelAdmin(admin.ModelAdmin):
    inlines = [ImagemImovelInline]
    list_display = ['titulo', 'cidade', 'bairro', 'categoria', 'tipo', 'visivel', 'codigo']
    search_fields = ['titulo', 'cidade__nome', 'bairro__nome', 'categoria__descricao', 'tipo__tipo', 'codigo',
                     'nome_proprietario', 'cpf_proprietario', 'nome_adquirinte', 'cpf_adquirinte', 'dataCobranca',
                     'tarja', ]
    list_filter = ('categoria__descricao', 'tipo__tipo', 'cidade', 'bairro', 'visivel', 'dataCobranca', 'tarja')

    list_per_page = 30
    fieldsets = (
        ('Imóvel', {
            'fields': (
                'titulo', 'descricao', 'codigo', 'data', 'preco', 'negociavel', 'troca', 'endereco', 'numero',
                'bairro', 'cidade', 'uf', 'quartos', 'garagem', 'banheiro', 'areaservico', 'churrasqueira', 'cozinha',
                'salajantar', 'sala', 'salaofesta', 'suites', 'categoria', 'tipo', 'imagem', 'tarja',
                'metragem_privativo', 'metragem_terreno', 'video', 'mensagem_adicional',
            )
        }),
        ('Financeiro', {
            'fields': (
                'visivel', 'dataCobranca', 'dataContrato', 'dataVenda', 'observacoes_internas',
            )
        }),
        ('Proprietário', {
            'fields': (
                'nome_proprietario', 'observacao_proprietario', 'fone_proprietario'
            )
        }),
    )


class InteressadosModelAdmin(admin.ModelAdmin):
    list_display = ['nome', 'telefone', 'email', 'get_titulo', 'get_codigo']
    search_fields = ['nome', 'telefone', 'email', 'imovel__titulo', 'imovel__codigo']
    list_filter = ('imovel__cidade__nome', 'imovel__bairro__nome', 'imovel__tipo', 'imovel__categoria',
                   'imovel__titulo', 'imovel__codigo')

    def get_titulo(self, obj):
        return obj.imovel.titulo

    def get_codigo(self, obj):
        return obj.imovel.codigo

    get_titulo.short_description = 'Imóvel'
    get_titulo.admin_order_field = 'imovel__titulo'

    get_codigo.short_description = 'Código Imóvel'
    get_codigo.admin_order_field = 'imovel__codigo'


class ContatosModelAdmin(admin.ModelAdmin):
    list_display = ['nome', 'telefone', 'email', 'mensagem']
    search_fields = ['nome', 'telefone', 'email', 'mensagem']
    list_filter = ('nome', 'telefone', 'email')


class InstitucionalModelAdmin(admin.ModelAdmin):
    list_display = ['whatsapp', 'whatsappsecundario', 'mensagem_imovel', ]
    search_fields = ['whatsapp', 'whatsappsecundario', ]
    list_filter = ('whatsapp', 'whatsappsecundario',)


class BannerModelAdmin(admin.ModelAdmin):
    list_display = ['nome', 'celular']


admin.site.register(Imovel, ImovelModelAdmin)
admin.site.register(Categoria, CategoriaModelAdmin)
admin.site.register(Cidade, CidadeModelAdmin)
admin.site.register(Bairro, BairroModelAdmin)
admin.site.register(Tipo, TipoModelAdmin)
admin.site.register(Interessados, InteressadosModelAdmin)
admin.site.register(Contato, ContatosModelAdmin)
admin.site.register(Institucional, InstitucionalModelAdmin)
admin.site.register(Banner, BannerModelAdmin)
admin.site.unregister(Group)
