from admin_reorder.middleware import ModelAdminReorder
from django.conf import settings
from django.urls import resolve, Resolver404


class CustomAdminReorder(ModelAdminReorder):
    def process_template_response(self, request, response):
        try:
            url = resolve(request.path_info)
        except Resolver404:
            return response
        if not url.app_name == 'admin' and \
                url.url_name not in ['index', 'app_list']:
            return response
        try:
            app_list = response.context_data['app_list']
        except KeyError:
            try:
                app_list = response.context_data['available_apps']
            except KeyError:
                return response
        self.init_config(request, app_list)
        ordered_app_list = self.get_app_list()
        ordered_app_list = self.set_icons(ordered_app_list)
        response.context_data['app_list'] = ordered_app_list
        return response

    def set_icons(self, ordered_app_list):
        apps_config = getattr(settings, 'ADMIN_REORDER', None)
        for app in ordered_app_list:
            for app_config in apps_config:
                if app['app_label'] == app_config['app']:
                    app['icon'] = app_config.get('icon', '')
                    break
        return ordered_app_list
