import uuid
import re

from cloudinary.models import CloudinaryField
from django.db import models
from geopy import geocoders

UF_CHOICES = (
    ('RS', 'Rio Grande do Sul'),
    ('SC', 'Santa Catarina')
)


class Categoria(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    descricao = models.CharField('categoria casa, apartamento, etc', max_length=50)

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'categoria'
        verbose_name_plural = 'categorias'
        db_table = 'categoria'


class Cidade(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nome = models.CharField('nome', max_length=50)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'cidade'
        verbose_name_plural = 'cidades'
        db_table = 'cidade'


class Bairro(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nome = models.CharField('nome', max_length=50)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'bairro'
        verbose_name_plural = 'bairros'
        db_table = 'bairro'
        ordering = ('nome',)


class Tipo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    tipo = models.CharField('tipo venda ou locação', max_length=50)

    def __str__(self):
        return self.tipo

    class Meta:
        verbose_name = 'tipo'
        verbose_name_plural = 'tipos'
        db_table = 'tipo'


class Imovel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data = models.DateField('data do anúncio')
    preco = models.DecimalField('valor', decimal_places=2, max_digits=15)
    endereco = models.CharField('endereço', max_length=300, blank=True, null=True)
    codigo = models.CharField('código imóvel', max_length=10, blank=True)
    numero = models.CharField('número', max_length=20, blank=True)
    bairro = models.ForeignKey(Bairro, on_delete=models.PROTECT, null=True)
    cidade = models.ForeignKey(Cidade, on_delete=models.PROTECT, null=True)
    uf = models.CharField('uf', max_length=2, choices=UF_CHOICES, blank=True, null=True)
    quartos = models.PositiveSmallIntegerField('dormitórios', default=0)
    suites = models.PositiveSmallIntegerField('suítes', default=0)
    salaofesta = models.PositiveSmallIntegerField('Salão de festas', default=0, blank=True)
    sala = models.PositiveSmallIntegerField('Sala de estar', default=0)
    salajantar = models.PositiveSmallIntegerField('Sala de jantar', default=0)
    cozinha = models.PositiveSmallIntegerField('Cozinha', default=0)
    churrasqueira = models.PositiveSmallIntegerField('Churrasqueira', default=0, blank=True)
    areaservico = models.PositiveSmallIntegerField('Area de serviço', default=0, blank=True)
    metragem_terreno = models.IntegerField('Metragem terreno', default=0)
    metragem_privativo = models.IntegerField('Metragem privativa', default=0)
    garagem = models.PositiveSmallIntegerField('vagas de garagem', default=0)
    banheiro = models.PositiveSmallIntegerField('banheiros', default=0)
    titulo = models.CharField('título', max_length=100)
    descricao = models.TextField('descrição', max_length=5000)
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT)
    tipo = models.ForeignKey(Tipo, on_delete=models.PROTECT)
    imagem = CloudinaryField('imagem de capa')
    visivel = models.BooleanField('mostrar no site', default=True)
    dataCobranca = models.CharField('dia de vencimento', blank=True, max_length=2)
    dataVenda = models.DateField('data da venda', blank=True, null=True)
    dataContrato = models.DateField('data de inicio do contrato', blank=True, null=True)
    nome_proprietario = models.CharField('nome do proprietário', max_length=200)
    fone_proprietario = models.CharField('Telefone do proprietário', max_length=200, null=True)
    observacao_proprietario = models.TextField('Observações do proprietário', blank=True)
    latitude = models.CharField('Latitude', max_length=20, blank=True)
    longitude = models.CharField('Longitude', max_length=20, blank=True)
    tarja = models.CharField('Tarja', max_length=300, blank=True, null=True)
    negociavel = models.BooleanField('Valor negociável', default=False)
    troca = models.CharField('Aceita troca por', max_length=300, blank=True, null=True)
    observacoes_internas = models.TextField('Observações internas', blank=True, null=True)
    video = models.CharField('Link do vídeo no Youtube', max_length=100, blank=True)
    cpf_adquirinte = models.CharField('não usado', blank=True, null=True, max_length=100)
    cpf_proprietario = models.CharField('não usado', blank=True, null=True, max_length=100)
    nome_adquirinte = models.CharField('nao usar', null=True, blank=True, max_length=100)
    mensagem_adicional = models.CharField('Mensagem adicional', null=True, blank=True, max_length=300)

    def __str__(self):
        return self.titulo

    def obterEndereco(self):
        if self.bairro and self.cidade and self.uf:
            return f'{self.bairro}, {self.cidade.nome} - {self.uf}'
        else:
            return ''

    def save(self, *args, **kwargs):
        try:
            g = geocoders.GoogleV3(api_key='AIzaSyByFCKk6PjOLP77cF2YfSrJogdDPYc2zug')
            input_address = self.obterEndereco()
            location = g.geocode(input_address, timeout=10)
            self.latitude = location.latitude
            self.longitude = location.longitude
        except Exception as e:
            print(e)
        try:
            super().save(*args, **kwargs)
        except Exception as e:
            print(e)

    def video_id(self):
        regex = re.compile(
            r'(https?://)?(www\.)?(youtube|youtu|youtube-nocookie)\.(com|be)/(watch\?v=|embed/|v/|.+\?v=)?(?P<id>[A-Za-z0-9\-=_]{11})')
        match = regex.match(self.video)
        video = match.group('id')
        return video

    class Meta:
        verbose_name = 'imóvel'
        verbose_name_plural = 'imóveis'
        db_table = 'imovel'


class ImagemImovel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    imagem = CloudinaryField('imagem de capa')
    imovel = models.ForeignKey(Imovel, verbose_name='imovel', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'imagem'
        verbose_name_plural = 'imagens'


class Interessados(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nome = models.CharField('nome', max_length=100)
    email = models.CharField('e-mail', max_length=100)
    telefone = models.CharField('telefone', max_length=100)
    imovel = models.ForeignKey(Imovel, verbose_name='imóvel', on_delete=models.PROTECT)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Interessados'
        verbose_name_plural = 'Interessados'


class Contato(models.Model):
    nome = models.CharField(max_length=100)
    telefone = models.CharField(blank=True, max_length=20)
    email = models.EmailField(blank=True, max_length=100)
    mensagem = models.TextField()

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Contatos'
        verbose_name_plural = 'Contatos'


class Institucional(models.Model):
    whatsapp = models.CharField('whatsapp principal', max_length=100)
    whatsappsecundario = models.CharField('whatsapp secundário', max_length=100)
    mensagem_imovel = models.CharField('mensagem adicional no imóvel', max_length=300, null=True, blank=True)

    def __str__(self):
        return self.whatsapp

    class Meta:
        verbose_name = 'institucional'
        verbose_name_plural = 'institucional'


class Banner(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nome = models.CharField(max_length=100)
    texto = models.CharField(max_length=500, null=True, blank=True)
    imagem = CloudinaryField('imagem de capa')
    url = models.URLField('Link', blank=True, null=True)
    celular = models.BooleanField('Versão para celular', blank=True, default=False)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Banner'
        verbose_name_plural = 'Banners'
