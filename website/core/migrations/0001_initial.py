# Generated by Django 2.0.5 on 2018-06-12 23:46

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='bairro',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('nome', models.CharField(max_length=50, verbose_name='nome')),
            ],
            options={
                'verbose_name': 'bairro',
                'verbose_name_plural': 'bairros',
                'db_table': 'bairro',
            },
        ),
        migrations.CreateModel(
            name='categoria',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('descricao', models.CharField(max_length=50, verbose_name='descrição')),
            ],
            options={
                'verbose_name': 'categoria',
                'verbose_name_plural': 'categorias',
                'db_table': 'categoria',
            },
        ),
        migrations.CreateModel(
            name='cidade',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('nome', models.CharField(max_length=50, verbose_name='nome')),
            ],
            options={
                'verbose_name': 'cidade',
                'verbose_name_plural': 'cidades',
                'db_table': 'cidade',
            },
        ),
        migrations.CreateModel(
            name='Contato',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('telefone', models.CharField(blank=True, max_length=20)),
                ('email', models.EmailField(blank=True, max_length=100)),
                ('mensagem', models.TextField()),
            ],
            options={
                'verbose_name': 'Contatos',
                'verbose_name_plural': 'Contatos',
            },
        ),
        migrations.CreateModel(
            name='ImagemImovel',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('imagem', models.ImageField(blank=True, upload_to='media/', verbose_name='imagem de capa')),
            ],
            options={
                'verbose_name': 'imagem',
                'verbose_name_plural': 'imagens',
            },
        ),
        migrations.CreateModel(
            name='imovel',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('data', models.DateField(verbose_name='data do anúncio')),
                ('preco', models.DecimalField(decimal_places=2, max_digits=15, verbose_name='valor')),
                ('endereco', models.CharField(max_length=300, verbose_name='endereço')),
                ('numero', models.CharField(blank=True, max_length=20, verbose_name='número')),
                ('cep', models.CharField(max_length=9, verbose_name='CEP')),
                ('uf', models.CharField(choices=[('AC', 'Acre'), ('AL', 'Alagoas'), ('AP', 'Amapá'), ('BA', 'Bahia'), ('CE', 'Ceará'), ('DF', 'Distrito Federal'), ('ES', 'Espírito Santo'), ('GO', 'Goiás'), ('MA', 'Maranão'), ('MG', 'Minas Gerais'), ('MS', 'Mato Grosso do Sul'), ('MT', 'Mato Grosso'), ('PA', 'Pará'), ('PB', 'Paraíba'), ('PE', 'Pernanbuco'), ('PI', 'Piauí'), ('PR', 'Paraná'), ('RJ', 'Rio de Janeiro'), ('RN', 'Rio Grande do Norte'), ('RO', 'Rondônia'), ('RR', 'Roraima'), ('RS', 'Rio Grande do Sul'), ('SC', 'Santa Catarina'), ('SE', 'Sergipe'), ('SP', 'São Paulo'), ('TO', 'Tocantins')], max_length=2, verbose_name='uf')),
                ('quartos', models.PositiveSmallIntegerField(default=0, verbose_name='quartos')),
                ('garagem', models.PositiveSmallIntegerField(default=0, verbose_name='vagas de garagem')),
                ('banheiro', models.PositiveSmallIntegerField(default=0, verbose_name='banheiros')),
                ('titulo', models.CharField(max_length=100, verbose_name='título')),
                ('descricao', models.TextField(verbose_name='descrição')),
                ('imagem', models.ImageField(blank=True, upload_to='media/', verbose_name='imagem de capa')),
                ('bairro', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, to='core.bairro')),
                ('categoria', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.categoria')),
                ('cidade', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, to='core.cidade')),
            ],
            options={
                'verbose_name': 'imovel',
                'verbose_name_plural': 'imoveis',
                'db_table': 'imovel',
            },
        ),
        migrations.CreateModel(
            name='Interessados',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('nome', models.CharField(max_length=100, verbose_name='nome')),
                ('email', models.CharField(max_length=100, verbose_name='e-mail')),
                ('telefone', models.CharField(max_length=100, verbose_name='telefone')),
                ('imovel', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.imovel', verbose_name='imovel')),
            ],
            options={
                'verbose_name': 'Interessados',
                'verbose_name_plural': 'Interessados',
            },
        ),
        migrations.CreateModel(
            name='tipo',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('tipo', models.CharField(max_length=50, verbose_name='tipo')),
            ],
            options={
                'verbose_name': 'tipo',
                'verbose_name_plural': 'tipos',
                'db_table': 'tipo',
            },
        ),
        migrations.AddField(
            model_name='imovel',
            name='tipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.tipo'),
        ),
        migrations.AddField(
            model_name='imagemimovel',
            name='imovel',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.imovel', verbose_name='imovel'),
        ),
    ]
