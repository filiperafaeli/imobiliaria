from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.urls import path, include
from django.contrib import admin


from website.core import views
from website.core.forms import BuscaForm

urlpatterns = [
    url('admin/', admin.site.urls),
    path('', views.Home.as_view(), name='home'),
    path('sobre/', views.sobre, name='sobre'),
    path('busca/', views.Busca, name='busca', kwargs={'form': BuscaForm}),
    url('contato/$', views.contato, name='contato'),
    path('detalhes/<str:id_imovel>', views.detalhes, name='detalhes'),
    path('lista/<str:tipo_imovel>/<str:categoria_imovel>', views.lista, name='lista'),
    path('todos/', views.todos, name='todos'),
    path('telao1/', views.telao1, name='telao1'),
    path('telao2/', views.telao2, name='telao2'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


admin.site.site_title = 'Imobiliária'
admin.site.site_header = 'Nome Imobiliaria'
admin.site.index_title = 'Painel de administração'

